#!/usr/bin/env perl

use warnings;
use strict;

use Spydyr;
use JSON::XS;

sub main
{
    my @args = @_;
    my $host = $args[0] // q{localhost};
    chomp($host);

    my $filters = [ ];

    # Instantiate the Spydyr
    my $spydyr = Spydyr->new( 'host' => $host, 'filters' => $filters );

    # Crawl the entire domain
    $spydyr->crawl();

    # Generate the sitemap
    my $sitemap = $spydyr->generate_sitemap();

    # Encode hashref as JSON
    my $json_sitemap = JSON::XS->new()->utf8()->pretty()->encode($sitemap);

    open( my $FH, '>', 'sitemap.json' )
        or die 'failed to open sitemap.json for writing';

    # Print the JSON sitemap
    print $FH $json_sitemap;

    close($FH);

    return 0;
}

# Run the Spydyr and exit
exit( main ( @ARGV ) );

#EOF
