#!/usr/bin/env perl

use warnings;
use strict;

use ExtUtils::MakeMaker;

WriteMakefile
    (
     'NAME'              => "Spydyr",
     'AUTHOR'            => "Charles Boyd",
     'VERSION_FROM'      => "lib/Spydyr.pm",
     'EXE_FILES'         => [ 'bin/spydyr.pl' ],
     'PREREQ_PM'         =>
     {
         'JSON::XS'        => "3.0.0",
         'WWW::Mechanize'  => "1.75",
         'Digest::SHA'     => 0
     },
    );
