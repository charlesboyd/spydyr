Spydyr
=====

A simple web crawler in modern Perl.

Description
-----

After installing, simply runnning the script ```bin/spydyr.pl``` with the domain to crawl as the only argument is all that needs to be done.

This should print the SHA1 hash of every link that gets indexed and dump a file ```sitemap.json``` in the current working directory containing a dependency graph of all pages indexed in the crawl.

Author
------

Charles Boyd <charles.boyd@yandex.com>
