package Spydyr;

use warnings;
use strict;

use Spydyr::Agent;
use Spydyr::Page;
use Spydyr::Filter;

our $VERSION='0.0.1';

sub new
{
    my ( $class, %p_options ) = @_;

    my $self =
    {
        'host'    => $p_options{'host'},
    };

    bless( $self, $class );

    my $agent = Spydyr::Agent->new( 'host' => $self->_host() );

    $self->{'agent'} = $agent;

    if ( defined( $p_options{'filters'} ) )
    {
        my @filters = map { Spydyr::Filter->new( 'regexp' => $_ ) } @{ $p_options{'filters'} };
        map { $self->_agent()->add_filter( $_ ) } @filters;
    }

    return $self;
}

sub _host
{
    my ( $self ) = @_;
    return $self->{'host'};
}

sub _agent
{
    my ( $self ) = @_;
    return $self->{'agent'};
}

sub start_at
{
    my ( $self, $uri ) = @_;

    my $start_page = Spydyr::Page->new( $uri );

    $self->_agent()->enqueue($start_page);

    while ( ! $self->_agent()->is_empty() )
    {
        $self->_agent()->get_next_page();
    }

    return $self->_agent()->get_pages();
}

sub crawl
{
    my ( $self ) = @_;
    return $self->start_at( $self->_host() );
}

sub generate_sitemap
{
    my ( $self ) = @_;

    my $pages_ref = $self->_agent()->get_pages();

    my $sitemap = {};

    # Build a hash from indexed pages arrayref
    foreach my $page ( @{ $pages_ref } )
    {
        my $hash = $page->get_hash();

        # Associate SHA1 hash to URL and arrayref of linked hashes
        $sitemap->{$hash} =
        {
            'url'           => $page->get_url(),
            'linked_hashes' => $page->get_linked_hashes()
        };
    }

    # Resolve the linked hashes to their associated URL
    foreach my $k ( keys( %{ $sitemap } ) )
    {
        my @linked_hashes = @{ delete($sitemap->{$k}->{'linked_hashes'}) };
        my @linked_urls = map { $sitemap->{$_}->{'url'} } @linked_hashes;
        $sitemap->{$k}->{'linked_urls'} = \@linked_urls;
    }

    return $sitemap;
}

1;
