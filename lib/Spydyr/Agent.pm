package Spydyr::Agent;

use warnings;
use strict;

use Spydyr::Page;

use WWW::Mechanize;

sub new
{
    my ( $class, %p_options ) = @_;

    my $onerror = sub { return 0; };

    my $self =
    {
        'mech'    => WWW::Mechanize->new( 'onerror' => $onerror ),
        'host'    => $p_options{'host'} // 'localhost',
        'filters' => $p_options{'filters'} // [],
        'queue'   => [],
        'pages'   => [],
        'seen'    => {},

    };

    bless( $self, $class );
    return $self;
}

sub _host
{
    my ( $self ) = @_;
    return $self->{'host'};
}

sub _mech
{
    my ( $self ) = @_;
    return $self->{'mech'};
}

sub _queue
{
    my ( $self ) = @_;
    return $self->{'queue'};
}

sub _seen
{
    my ( $self ) = @_;
    return $self->{'seen'};
}

sub _pages
{
    my ( $self ) = @_;
    return $self->{'pages'};
}

sub _filters
{
    my ( $self ) = @_;
    return $self->{'filters'};
}

sub get_filters
{
    my ( $self ) = @_;
    return $self->_filters();
}

sub get_pages
{
    my ( $self ) = @_;
    return $self->_pages();
}

sub get_host
{
    my ( $self ) = @_;
    return $self->_host();
}

sub enqueue
{
    my ( $self, $page ) = @_;

    if ( ! defined($page) || ref($page) ne 'Spydyr::Page' )
    {
        warn q{Attempted to put an unexpected type on the queue};
        return undef;
    }

    if ( ! $self->is_seen( $page->get_hash() ) )
    {
        push( @{ $self->_queue() }, $page );
    }

    return $page;
}

sub dequeue
{
    my ( $self ) = @_;
    return pop( @{ $self->_queue() } );
}

sub is_empty
{
    my ( $self ) = @_;

    if ( scalar( @{ $self->_queue() } ) <= 0 )
    {
        return 1;
    }

    return 0;
}

sub add_filter
{
    my ( $self, $filter ) = @_;
    push( @{ $self->_filters() }, $filter );
    return 1;
}

sub apply_filters
{
    my ( $self, $link ) = @_;

    my $url = $link->url_abs()->as_string();

    foreach my $filter ( @{ $self->get_filters() } )
    {
        my $filter_result = $filter->apply( $url );

        if ( ! defined($filter_result ) )
        {
            return undef;
        }
    }

    print $url,"\n";

    return $link;
}

sub is_seen
{
    my ( $self, $sha1 ) = @_;

    if ( $self->_seen()->{$sha1} )
    {
        return 1;
    }

    return 0;
}

sub get_next_page
{
    my ( $self ) = @_;

    # Stop running when the queue is empty
    if ( $self->is_empty() )
    {
        warn q{Queue is empty, nothing to do!};
        return 0;
    }

    # Get the next page we intend to process off the queue
    my $next_page = $self->dequeue();

    # Unique key for the page to store in associative array later
    my $sha1 = $next_page->get_hash();

    # Move on if we have seen this page before
    if ( $self->is_seen( $sha1 ) )
    {
        return 0;
    }
    else
    {
        # Note that we have seen this page
        $self->_seen()->{$sha1} = 1;

        # Send a GET request to the next page
        my $res = $self->_mech()->get( $next_page->get_url() );

        # Print a warning if we do not get a good response from server
        if ( $self->_mech()->status() != 200 )
        {
            warn qq{Failed to fetch page with status:} . $self->_mech()->status();
            return 0;
        }

        # Extract the links and apply our filters
        my @links = map { $self->apply_filters( $_ ) } @{ $self->_mech()->links() };

        # Enqueue the the pages we have not yet seen
        my @pages = map { $self->enqueue( Spydyr::Page->new( $_->url_abs()->as_string() ) ) if defined $_ } @links;

        # Print info when we get each page
        print q{Indexing: } . $next_page->get_hash(),"\n";

        # Insert dependency links
        foreach my $p ( @pages )
        {
            if ( ref($p) eq 'Spydyr::Page' )
            {
                $next_page->add_link($p);
            }
        }

        # Add the Page object to our list of visited pages
        push( @{ $self->_pages() }, $next_page );
    }

    return 1;
}

1;
