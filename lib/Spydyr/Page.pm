package Spydyr::Page;

use warnings;
use strict;

use Digest::SHA qw( sha1_hex );

sub new
{
    my ( $class, $link ) = @_;

    my $strip_url = $link;
    $strip_url =~ s/(http|https):\/\///g;
    my @components = split( '/', $strip_url );
    shift(@components);

    my $self =
    {
        'url'        => $link,
        'components' => \@components,
        'links_to'   => [],
    };

    bless( $self, $class );

    $self->{'sha1'} = sha1_hex( $self->get_url() );
    $self->{'depth'} = scalar(@components);
    $self->{'components'} = \@components;

    return $self;
}

sub _sha1
{
    my ( $self ) = @_;
    return $self->{'sha1'};
}

sub _links_to
{
    my ( $self ) = @_;
    return $self->{'links_to'};
}

sub _url
{
    my ( $self ) = @_;
    return $self->{'url'};
}

sub _components
{
    my ( $self ) = @_;
    return $self->{'components'};
}

sub _depth
{
    my ( $self ) = @_;
    return $self->{'depth'};
}

sub get_hash
{
    my ( $self ) = @_;
    return $self->_sha1();
}

sub get_url
{
    my ( $self ) = @_;
    return $self->_url();
}

sub get_components
{
    my ( $self ) = @_;
    return $self->_components();
}

sub get_depth
{
    my ( $self ) = @_;
    return $self->_depth();
}

sub get_linked_hashes
{
    my ( $self ) = @_;
    return $self->_links_to();
}

sub add_link
{
    my ( $self, $page ) = @_;
    push ( @{ $self->_links_to() }, $page->get_hash() );
}

1;
