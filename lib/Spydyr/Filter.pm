package Spydyr::Filter;

use warnings;
use strict;

sub new
{
    my ( $class, %p_args ) = @_;

    my $self =
    {
        'regexp' => $p_args{'regexp'},
    };

    bless( $self, $class );
    return $self;
}

sub _regexp
{
    my ( $self ) = @_;
    return $self->{'regexp'};
}

sub apply
{
    my ( $self, $link ) = @_;

    my $regexp = $self->_regexp();

    if ( $link =~ m/$regexp/g )
    {
        return $link;
    }

    return undef;
}

1;
